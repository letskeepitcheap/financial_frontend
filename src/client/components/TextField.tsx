import React, { useMemo } from "react";
import { useState } from "react";

interface ITextFieldProps {
    preToken?: string;
    password?: boolean;
    validator?: (value: string) => boolean;
    disabled?: boolean;
    value: string;
    label: string;
    onChange: (value: string) => void;
}

const TextField = (props: ITextFieldProps) => {
    const [focused, setFocused] = useState(false);
    const [error, setError] = useState(false);

    const id = `text-${props.label}`;
    const onClick = () => document.getElementById(id)?.focus();

    const containerClasses = useMemo(() => {
        const classes: string[] = ['text-field'];
        if (focused || props.value || props.preToken) { classes.push('active'); }
        if (props.preToken) { classes.push('has-pre-token'); }
        if (props.disabled) { classes.push('disabled'); }
        if (error) { classes.push('error'); }
        return classes.join(' ');
    }, [focused, props.value, props.preToken, props.disabled, error]);


    const labelClasses = useMemo(() => {
        const classes: string[] = [];
        if (focused || props.value || props.preToken) { classes.push('active'); }
        return classes.join(' ');
    }, [focused, props.value, props.preToken]);

    const type = useMemo(() => props.password ? 'password' : 'text', [props.password]);

    const validate = () => {
        if (props.validator) {
            setError(props.validator(props.value));
        }
    }


    return (
        <div id={`text-field-${props.label}`} className={containerClasses} onClick={onClick} onFocus={() => setFocused(true)} onBlur={() => setFocused(false)}>
            <label className={labelClasses} >{props.label}</label>
            <span className='text-pre'>{props.preToken ? props.preToken[0] : ''}</span>
            <input id={id} type={type} value={props.value} disabled={props.disabled} onBlur={validate} onChange={event => props.onChange(event.target.value)} />
        </div>
    )
}

export default TextField;