import { AnyAction } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import IFrequency from "../../models/IFrequency";
import ITransactionType from "../../models/ITransactionType";
import IIncomeGeneratorRequest from "../../models/requests/IIncomeGeneratorRequest";
import IIncomeGenerator from "../../models/IIncomeGenerator";
import ILedgerEntry from "../../models/ILedgerEntry";
import IExpense from "../../models/IExpense";
import ILedgerEntryRequest from "../../models/requests/ILedgerEntryRequest";
import IExpenseRequest from "../../models/requests/IExpenseRequest";
import IDatespanRequest from "../../models/requests/IDatespanRequest";
import { LedgerAction, StoreAction } from "../actions";
import AppDataState from "../AppDataState";
import { del, get, post } from "../../utilities/backend_client";
import { NULL_ACTION } from "../../utilities/constants";

// TODO (alexa): This is never used. Figure out what to do with it.
const setLedgerError = (message: string): StoreAction => ({ type: LedgerAction.SET_LEDGER_ERROR, payload: { errorMessage: message } });
const setCategories = (categories: string[]): StoreAction => ({ type: LedgerAction.SET_CATEGORIES, payload: { categories: categories } });

export const getCategories = (partial: string): ThunkAction<Promise<void>, AppDataState, undefined, AnyAction> =>
    async (dispatch: ThunkDispatch<AppDataState, undefined, AnyAction>): Promise<void> => {
        if (partial) {
            const response = await get(`ledger/categories/${partial}`, setCategories, setLedgerError)
            dispatch(response);
        } else {
            dispatch(setCategories([]))
        }
    }

const setFrequencies = (frequencies: IFrequency[]): StoreAction => ({ type: LedgerAction.SET_FREQUENCIES, payload: { frequencies: frequencies } });

export const getFrequencies = (): ThunkAction<Promise<void>, AppDataState, undefined, AnyAction> =>
    async (dispatch: ThunkDispatch<AppDataState, undefined, AnyAction>): Promise<void> => {
        const response = await get('ledger/frequencies', setFrequencies, setLedgerError)
        dispatch(response);
    }

const setTransactionTypes = (types: ITransactionType[]): StoreAction => ({ type: LedgerAction.SET_TRANSACTION_TYPES, payload: { transactionTypes: types } });

export const getTransactionTypes = (): ThunkAction<Promise<void>, AppDataState, undefined, AnyAction> =>
    async (dispatch: ThunkDispatch<AppDataState, undefined, AnyAction>): Promise<void> => {
        const response = await get('ledger/transactiontypes', setTransactionTypes, setLedgerError)
        dispatch(response);
    }

export const addIncomeGenerator = (request: IIncomeGeneratorRequest): ThunkAction<Promise<void>, AppDataState, undefined, AnyAction> =>
    async () => { await post(request, 'ledger/generator', NULL_ACTION, setLedgerError); }

const setIncomeGenerators = (generators: IIncomeGenerator[]): StoreAction => ({
    type: LedgerAction.SET_INCOME_GENERATORS,
    payload: {
        incomeGenerators: generators.map(g => ({
            ...g,
            recurringTransactions: g.recurringTransactions.map(t => ({
                ...t,
                lastTriggered: new Date(t.lastTriggered)
            }))
        }))
    }
});

export const getIncomeGenerators = (): ThunkAction<Promise<void>, AppDataState, undefined, AnyAction> =>
    async (dispatch: ThunkDispatch<AppDataState, undefined, AnyAction>): Promise<void> => {
        const response = await get('ledger/generators', setIncomeGenerators, setLedgerError)
        dispatch(response);
    }

const setLedgerEntries = (entries: ILedgerEntry[]): StoreAction => ({
    type: LedgerAction.SET_LEDGER_ENTRIES,
    payload: {
        entries: entries.map(x => ({
            ...x,
            // Dates are parsed as strings by default.
            transactionDate: new Date(x.transactionDate)
        }))
    }
});

export const getLedgerEntries = (request: IDatespanRequest): ThunkAction<Promise<void>, AppDataState, undefined, AnyAction> =>
    async (dispatch: ThunkDispatch<AppDataState, undefined, AnyAction>): Promise<void> => {
        const response = await get(`ledger/${request.start.getTime()}/${request.end.getTime()}`, setLedgerEntries, setLedgerError);
        dispatch(response);
    }

export const addLedgerEntry = (request: ILedgerEntryRequest): ThunkAction<Promise<void>, AppDataState, undefined, AnyAction> =>
    async () => { await post(request, 'ledger', NULL_ACTION, setLedgerError); }

const setExpenses = (expenses: IExpense[]): StoreAction => ({
    type: LedgerAction.SET_EXPENSES,
    payload: {
        expenses: expenses.map(x => ({
            ...x,
            // Dates are parsed as strings by default.
            lastTriggered: new Date(x.lastTriggered)
        }))
    }
});

export const getExpenses = (): ThunkAction<Promise<void>, AppDataState, undefined, AnyAction> =>
    async (dispatch: ThunkDispatch<AppDataState, undefined, AnyAction>): Promise<void> => {
        const response = await get('ledger/recurringtransactions', setExpenses, setLedgerError);
        dispatch(response);
    }

export const addExpense = (request: IExpenseRequest): ThunkAction<Promise<void>, AppDataState, undefined, AnyAction> =>
    async () => { await post(request, 'ledger/recurringtransaction', NULL_ACTION, setLedgerError); }

const removeIncomeGenerator = (id: string): StoreAction => ({
    type: LedgerAction.REMOVE_INCOME_GENERATOR,
    payload: { id }
});

export const deleteIncomeGenerator = (id: string): ThunkAction<void, AppDataState, undefined, AnyAction> =>
    async (dispatch: ThunkDispatch<AppDataState, undefined, AnyAction>) => {
        await del(`ledger/generator/${id}`);
        dispatch(removeIncomeGenerator(id));
    }

const removeLedgerEntry = (id: string): StoreAction => ({
    type: LedgerAction.REMOVE_LEDGER_ENTRY,
    payload: { id }
});

export const deleteLedgerEntry = (id: string): ThunkAction<void, AppDataState, undefined, AnyAction> =>
    async (dispatch: ThunkDispatch<AppDataState, undefined, AnyAction>): Promise<void> => {
        await del(`ledger/${id}`);
        dispatch(removeLedgerEntry(id));
    }

const removeExpense = (id: string): StoreAction => ({
    type: LedgerAction.REMOVE_EXPENSE,
    payload: { id }
});

export const deleteExpense = (id: string): ThunkAction<void, AppDataState, undefined, AnyAction> =>
    async (dispatch: ThunkDispatch<AppDataState, undefined, AnyAction>): Promise<void> => {
        await del(`ledger/recurringtransaction/${id}`);
        dispatch(removeExpense(id));
    }
