export default interface IFrequency {
    id: string;
    description: string;
    approxTimesPerYear: number;
}