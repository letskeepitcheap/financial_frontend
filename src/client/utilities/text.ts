export const getDisplayText = (category: string, description?: string): string =>
    !description ? category : `${category} (${description})`;
