import React, { useMemo } from "react"
import Dropdown, { IDropdownOption } from "../components/Dropdown"
import TextField from "../components/TextField"
import DateField from "../components/DateField"
import { push } from "connected-react-router"
import { useState } from "react"
import { connect } from "react-redux"
import AutocompleteField from "../components/AutocompleteField"
import { addExpense, getCategories, getTransactionTypes } from "../store/ledger/actions"
import { getDateFromFrequency } from "../utilities/dates"
import { categoryValidator, numericValidator } from "../utilities/validators"
import ITransactionType from "../models/ITransactionType"
import IFrequency from "../models/IFrequency"
import { usesTransactionTypes } from "../utilities/hooks"
import AppDataState from "../store/AppDataState"
import Button from "../components/Button"

// @ts-expect-error - The typescript compiler says that it can't find 
// the module for this import. Webpack still compiles it just fine.
import add from '../assets/add.png';

interface IAddExpenseProps {
    categories: string[];
    frequencies: IFrequency[];
    transactionTypes: ITransactionType[];
    getCategories: typeof getCategories;
    getTransactionTypes: typeof getTransactionTypes;
    addExpense: typeof addExpense;
    push: typeof push;
}

const AddExpense = (props: IAddExpenseProps) => {
    const [frequency, setFrequency] = useState('');
    const [transactionType, setTransactionType] = useState('');
    const [category, setCategory] = useState('');
    const [description, setDescription] = useState('');
    const [amount, setAmount] = useState('');
    const [lastTriggered, setLastTriggered] = useState(new Date());

    usesTransactionTypes(props.transactionTypes, props.getTransactionTypes);

    // Minimum date is dependent on the selected frequency.
    const minDate = useMemo(() => getDateFromFrequency(frequency, props.frequencies), [frequency, props.frequencies])
    const frequencies = useMemo(() => props.frequencies.map(x => ({ displayValue: x.description, value: x.id })), [props.frequencies]);
    const transactionTypes = useMemo(() => props.transactionTypes.map(x => ({ displayValue: x.description, value: x.id })), [props.transactionTypes]);

    const handleDateChanged = (date: Date | [Date, Date] | null) => {
        if (date instanceof Date) {
            setLastTriggered(date);
        }
    }

    const addDisabled = useMemo(() => {
        const numberAmount: number = parseFloat(amount);
        return !transactionType
            || !category
            // Description is optional.
            || isNaN(numberAmount) || numberAmount <= 0
            || !frequency
            || !lastTriggered;
    }, [amount, transactionType, category, frequency, lastTriggered]);

    const onAddClick = async () => {
        await props.addExpense({
            category: category,
            description: description,
            amount: parseFloat(amount),
            frequencyId: frequency,
            transactionTypeId: transactionType,
            lastTriggered: lastTriggered
        });
        props.push('/dashboard');
    }

    return (
        <div className="add-expense">
            <h1 className="title">Add expense</h1>

            <Dropdown label="Transaction type" value={transactionType} options={transactionTypes} onChange={setTransactionType} />
            <AutocompleteField label="Category" value={category} options={props.categories} onChange={setCategory} getOptions={props.getCategories} validator={categoryValidator} />
            <TextField label="Description" value={description} onChange={setDescription} />
            <TextField label="Amount" validator={numericValidator} preToken="$" value={amount} onChange={setAmount} />
            <Dropdown label="Frequency" value={frequency} options={frequencies} onChange={setFrequency} />
            <DateField label="Last executed" value={lastTriggered} onChange={handleDateChanged} minDate={minDate} maxDate={new Date()} />

            <div className="add">
                <Button onClick={onAddClick} disabled={addDisabled}>
                    <img src={add} alt="Add" />
                </Button>
            </div>
        </div>
    )
}

const mapStateToProps = (state: AppDataState): Partial<IAddExpenseProps> => {
    return {
        categories: state.ledger.categories,
        frequencies: state.ledger.frequencies,
        transactionTypes: state.ledger.transactionTypes
    };
}

export default connect(mapStateToProps, { getCategories, getTransactionTypes, addExpense, push })(AddExpense as any);