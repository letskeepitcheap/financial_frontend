import { LedgerState } from "./ledger/reducer";
import { UserState } from "./user/reducer";

export default interface AppDataState {
    router: any,
    ledger: LedgerState;
    user: UserState;
}