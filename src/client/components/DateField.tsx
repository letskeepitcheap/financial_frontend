import React from 'react';
import { Component } from 'react';
import Datepicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

interface IDateFieldProps {
    value: Date;
    label: string;
    disabled?: boolean;
    minDate?: Date;
    maxDate?: Date;
    onChange: (date: Date | [Date, Date] | null) => void;
}

const DateField = (props: IDateFieldProps): JSX.Element =>
    <Datepicker selected={props.value} disabled={props.disabled} minDate={props.minDate} maxDate={props.maxDate} onChange={(date) => props.onChange(date)} customInput={<CustomInput label={props.label} />} />

export default DateField;

interface ICustomInputProps {
    value?: string;
    label: string;
    disabled?: boolean;
    onClick?: () => void;
}

interface ICustomInputState {
    focused: boolean;
    id: string;
}

class CustomInput extends Component<ICustomInputProps, ICustomInputState> {
    constructor(props) {
        super(props);
        this.state = {
            focused: false,
            id: `date-field-${props.label}`
        };
    }

    setFocused = (value: boolean) => {
        this.setState({ focused: value });
    }

    onClick = () => {
        if (this.props.onClick) {
            this.props.onClick();
        }
    }

    getClasses = (base: string): string => {
        const classes: string[] = [base];
        if (this.state.focused || this.props.value) { classes.push('active'); }
        return classes.join(' ');
    }

    render() {
        return (
            <div className={this.getClasses('date-field')} onClick={() => this.onClick()} onFocus={() => this.setFocused(true)} onBlur={() => this.setFocused(false)}>
                <label className={this.getClasses('')} >{this.props.label}</label>
                <input readOnly id={this.state.id} type="text" disabled={this.props.disabled} value={this.props.value} />
            </div>
        );
    }
}