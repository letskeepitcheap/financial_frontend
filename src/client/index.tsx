import ReactDOM from 'react-dom';
import React from 'react';
import App from './App';
import { ConnectedRouter } from 'connected-react-router';
import { history } from './store/history';
import { Provider } from 'react-redux';
import store from './store/store';

// TODO (alexa): I really only very half-heartedly implemented SSR
// for this project, which means it's incomplete and buggy.

ReactDOM.hydrate(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App />
        </ConnectedRouter>
    </Provider>, document.getElementById('root'));
