import { push } from "connected-react-router";
import { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import React from "react";
import IFrequency from "../../models/IFrequency";
import IExpense from "../../models/IExpense";
import IIncomeGenerator from "../../models/IIncomeGenerator";
import ILedgerEntry from "../../models/ILedgerEntry";
import { deleteLedgerEntry, getLedgerEntries } from "../../store/ledger/actions";
import { getTotalIncomeGenerators } from "../../utilities/income_generators";
import { getTotalExpenses } from "../../utilities/expenses";
import { MONTHS } from "../../utilities/constants";
import Dropdown, { IDropdownOption } from "../Dropdown";
import { getFirstDayOfMonth, getLastDayOfMonth } from "../../utilities/dates";
import AppDataState from "../../store/AppDataState";
import { getDisplayText } from "../../utilities/text";

// @ts-expect-error - The typescript compiler says that it can't find 
// the module for this import. Webpack still compiles it just fine.
import add from '../../assets/add.png';
import LedgerEntryModal from "./LedgerEntryModal";

interface ILedgerHistoryProps {
    ledgerEntries: ILedgerEntry[];
    incomeGenerators: IIncomeGenerator[];
    expenses: IExpense[];
    frequencies: IFrequency[];
    getLedgerEntries: typeof getLedgerEntries;
    deleteLedgerEntry: typeof deleteLedgerEntry;
    push: typeof push;
}

const LedgerHistory = (props: ILedgerHistoryProps) => {
    const [month, setMonth] = useState(0);
    const [entry, setEntry] = useState<ILedgerEntry | undefined>(undefined);

    const ledgerEntriesId = 'ledger-history-entries-id';

    useEffect(() => {
        const entries = document.getElementById(ledgerEntriesId);
        if (entries) {
            entries.scrollTop = entries?.scrollHeight;
        }
    });

    const total = useMemo(() => {
        if (props.ledgerEntries.length === 0) {
            return 0;
        }
        return props.ledgerEntries
            .map(x => x.transactionType === 'Income' ? x.amount : -x.amount)
            .reduce((sum, x) => sum + x);
    }, [props.ledgerEntries]);


    const budgetText = useMemo(() => {
        const calculateTotalEntries = (): number => {
            const isRecurringTransaction = (entry: ILedgerEntry): boolean => {
                // If the entry is not marked as a recurring transaction, it's obviously
                // not a recurring transaction.
                if (!entry.recurringTransactionId) {
                    return false;
                }
                // If the entry _is_ marked as a recurring transaction, it might be linked
                // to an expense that has since been deleted, in which case it needs to be
                // included as though it is _not_ a recurring transaction because the list
                // of recurring transactions will no longer include it.
                const expenses: string[] = [
                    ...props.incomeGenerators.map(x => x.recurringTransactions.map(y => y.id)).flat(),
                    ...props.expenses.map(x => x.id)
                ]
                return expenses.includes(entry.recurringTransactionId);
            }

            if (props.ledgerEntries.length === 0) {
                return 0;
            }
            return props.ledgerEntries.filter(x => !isRecurringTransaction(x))
                .map(x => x.transactionType === 'Income' ? x.amount : -x.amount)
                .reduce((sum, x) => sum + x, 0);
        }

        if (month === 0) {
            const { net } = getTotalIncomeGenerators(props.incomeGenerators, props.frequencies, true);
            const recurringTransactions: number = getTotalExpenses(props.expenses, props.frequencies, true);
            const transactions: number = calculateTotalEntries();
            const total = Math.max(net + recurringTransactions + transactions, 0);

            return `Remaining budget for ${MONTHS[new Date().getMonth()]}: $${total.toFixed(2)}`;
        }
        return 'No budget information available for this month.';
    }, [props.ledgerEntries, props.incomeGenerators, props.expenses, props.frequencies, month]);

    const entries = useMemo(() => {
        let lastDate: Date | undefined = undefined;
        const elements: JSX.Element[] = [];

        props.ledgerEntries.forEach(x => {
            if (!lastDate || x.transactionDate.getDay() !== lastDate.getDay()) {
                elements.push(<div className="date">{x.transactionDate.toReadable()}</div>);
                lastDate = x.transactionDate;
            }
            elements.push(<LedgerEntry key={x.id} entry={x} onClick={setEntry} />)
        });
        return elements;
    }, [props.ledgerEntries]);

    const monthOptions = useMemo(() => {
        const options: IDropdownOption<string>[] = [];
        for (let i = 0; i < 12; i++) {
            const date: Date = new Date();
            date.setMonth(date.getMonth() - i);
            options.push({ value: i.toString(), displayValue: `${MONTHS[date.getMonth()]}, ${date.getFullYear()}` });
        }
        return options;
    }, []);

    const setMonthString = (value: string) => {
        const numberValue: number = parseInt(value);
        if (!isNaN(numberValue)) {
            setMonth(numberValue);
            let date: Date = new Date();
            date = new Date(date.getFullYear(), date.getMonth() - numberValue, date.getDate());
            props.getLedgerEntries({
                start: getFirstDayOfMonth(date.getFullYear(), date.getMonth()),
                end: getLastDayOfMonth(date.getFullYear(), date.getMonth())
            });
        }
    }

    const calculateTotalClasses = (): string => {
        const classes: string[] = ['value'];
        if (total < 0) { classes.push('negative'); }
        return classes.join(' ');
    }

    return (
        <div className="ledger-history">
            <h1 className="title">Ledger</h1>
            <div className="budget">
                {budgetText}
            </div>
            <div className="add-transaction">
                <img src={add} alt="Add" onClick={() => props.push('ledger/add')} />
            </div>
            <div className="month-selector">
                <Dropdown label="Month" value={month.toString()} options={monthOptions} onChange={setMonthString} />
            </div>
            <div id={ledgerEntriesId} className="entries">
                {entries}
            </div>
            <div className="total">
                <span className={calculateTotalClasses()}>${total.toFixed(2)}</span>
                <span className="text">Total</span>
            </div>
            <LedgerEntryModal entry={entry} deleteLedgerEntry={props.deleteLedgerEntry} close={() => setEntry(undefined)} />
        </div>
    );
}

const mapStateToProps = (state: AppDataState): Partial<ILedgerHistoryProps> => {
    return {
        ledgerEntries: state.ledger.ledgerEntries,
        incomeGenerators: state.ledger.incomeGenerators,
        expenses: state.ledger.expenses,
        frequencies: state.ledger.frequencies
    };
}

export default connect(mapStateToProps, { getLedgerEntries, deleteLedgerEntry, push })(LedgerHistory as any);

interface LedgerEntryProps {
    entry: ILedgerEntry;
    onClick: (value: ILedgerEntry) => void;
}

const LedgerEntry = (props: LedgerEntryProps) => {

    const getAmountClasses = (): string => {
        const classes: string[] = ['amount'];
        if (props.entry.transactionType !== "Income") { classes.push('negative'); }
        return classes.join(' ');
    }

    return (
        <div className="ledger-entry" onClick={() => props.onClick(props.entry)}>
            <span className="category">{getDisplayText(props.entry.category, props.entry.description)}</span>
            <span className={getAmountClasses()}>${props.entry.amount.toFixed(2)}</span>
        </div>
    );
}