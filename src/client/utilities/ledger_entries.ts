import ILedgerEntry from "../models/ILedgerEntry";

export const sortLedgerEntries = (array: ILedgerEntry[]): ILedgerEntry[] =>
    [...array].sort((a, b) => {
        if (a.transactionDate > b.transactionDate) {
            return 1;
        }
        if (b.transactionDate > a.transactionDate) {
            return -1;
        }
        return 0;
    });
