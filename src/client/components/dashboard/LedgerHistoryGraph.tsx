import { Pie } from 'react-chartjs-2';
import { useMemo, useState } from 'react';
import { connect } from 'react-redux';
import RadioButton, { IRadioOption } from '../RadioButton';
import React from 'react';
import ILedgerEntry from '../../models/ILedgerEntry';
import { CHART_COLORS } from '../../utilities/constants';
import AppDataState from '../../store/AppDataState';

interface LedgerHistoryGraphProps {
    ledgerEntries: ILedgerEntry[]
}

const LedgerHistoryGraph = (props: LedgerHistoryGraphProps) => {
    const [graph, setGraph] = useState('Expenditure');

    const data = useMemo(() => {
        const getLabelsAndValues = (): [string[], number[], string[]] => {
            const data: Record<string, number> = {};
            for (const entry of props.ledgerEntries) {
                if (entry.transactionType === graph) {
                    const key = entry.category;
                    if (data[key]) {
                        // If the key already exists in the record, add the amount
                        // to the existing amount in the record.
                        data[key] += entry.amount;
                    } else {
                        // If the key would be new, set the record to this amount.
                        data[key] = entry.amount;
                    }
                }
            }
            const dataList: { label: string, value: number, color: string }[] = [];
            let colorIndex = 0;
            for (const key in data) {
                dataList.push({
                    label: key,
                    value: data[key],
                    color: CHART_COLORS[colorIndex]
                });
                colorIndex = (colorIndex + 11) % CHART_COLORS.length;
            }
            dataList.sort((a, b) => {
                if (a.value > b.value) {
                    return -1;
                }
                if (b.value > a.value) {
                    return 1;
                }
                return 0;
            });
            return [dataList.map(x => x.label), dataList.map(x => x.value), dataList.map(x => x.color)];
        }

        const [labels, values, colors] = getLabelsAndValues();
        return {
            labels: labels,
            datasets: [
                {
                    data: values,
                    backgroundColor: colors,
                    borderWidth: 0
                }
            ]
        }
    }, [graph, props.ledgerEntries]);

    const graphOptions = {
        title: {
            display: false,
        },
        scales: {
            x: {
                grid: {
                    display: false,
                    drawBorder: false
                },
                ticks: { display: false }
            },
            y: {
                grid: {
                    display: false,
                    drawBorder: false
                },
                ticks: { display: false }
            }
        },
        legend: {
            display: true,
        },
        responsive: true,
        maintainAspectRatio: true
    };


    const options: IRadioOption<string>[] = [
        { value: 'Income', description: 'Income' },
        { value: 'Expenditure', description: 'Expenditures' }
    ];

    return (
        <div className="ledger-history-graph">
            <Pie height={300} width={300} data={data} options={graphOptions} />
            <RadioButton value={graph} options={options} onChange={(value) => setGraph(value)} />
        </div>
    );
}

const mapStateToProps = (state: AppDataState): LedgerHistoryGraphProps => {
    return {
        ledgerEntries: state.ledger.ledgerEntries
    };
}

export default connect(mapStateToProps, {})(LedgerHistoryGraph);