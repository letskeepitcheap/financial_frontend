import { useEffect } from "react";
import IFrequency from "../models/IFrequency";
import ITransactionType from "../models/ITransactionType";
import IDatespanRequest from "../models/requests/IDatespanRequest";
import { getFirstDayOfMonth, getLastDayOfMonth } from "./dates";

export const usesTransactionTypes = (transactionTypes: ITransactionType[], getTransactionTypes: () => void) => {
    useEffect(() => {
        // Only retrieve transaction types if we haven't already.
        if (transactionTypes.length === 0) {
            getTransactionTypes();
        }
    }, [transactionTypes, getTransactionTypes]);
}

export const usesFrequencies = (frequencies: IFrequency[], getFrequencies: () => void) => {
    useEffect(() => {
        // Only retrieve frequencies if we haven't already.
        if (frequencies.length === 0) {
            getFrequencies();
        }
    }, [frequencies, getFrequencies]);
}

export const usesIncomeGenerators = (getIncomeGenerators: () => void) => {
    useEffect(() => {
        getIncomeGenerators();
    }, [getIncomeGenerators]);
}

export const usesLedgerEntries = (getLedgerEntries: (request: IDatespanRequest) => void) => {
    useEffect(() => {
        const date: Date = new Date();
        getLedgerEntries({
            start: getFirstDayOfMonth(date.getFullYear(), date.getMonth()),
            end: getLastDayOfMonth(date.getFullYear(), date.getMonth())
        });
    }, [getLedgerEntries]);
}

export const usesExpenses = (getExpenses: () => void) => {
    useEffect(() => {
        getExpenses();
    }, [getExpenses]);
}

