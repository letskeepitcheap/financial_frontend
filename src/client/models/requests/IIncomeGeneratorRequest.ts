import IExpenseRequest from "./IExpenseRequest";

export default interface IIncomeGeneratorRequest {
    description: string;
    frequencyId: string;
    recurringTransactions: IExpenseRequest[];
}