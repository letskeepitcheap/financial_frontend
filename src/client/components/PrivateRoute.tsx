import React, { useMemo } from "react";
import { Redirect, Route, RouteProps, useLocation } from "react-router";
import { IUserInfo } from "../models/IUserInfo";
import UserRole from "../models/UserRole";
import { setRoute } from "../store/user/actions";

// TODO (alexa): This worked for CSR, but can be removed once the migration to SSR
// is completed.

interface PrivateRouteProps extends RouteProps {
    admin?: boolean;
    user: IUserInfo;
    setRoute: typeof setRoute;
}

const PrivateRoute = (props: PrivateRouteProps) => {
    const currentLocation = useLocation();

    const permitted = useMemo(() => {
        return props.admin ? props.user.role === UserRole.Admin : props.user !== undefined
    }, [props.admin, props.user]);

    if (permitted) {
        return <Route {...props} />
    } else {
        setRoute(currentLocation.pathname);
        const renderComponent = () => (<Redirect to={{ pathname: '/login' }} />);
        return <Route {...props} component={renderComponent} render={undefined} />
    }
}

export default PrivateRoute;