import express from 'express';
import cors from 'cors';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import path from 'path';
import fs from 'fs';
import App from './client/App';
import { ConnectedRouter } from 'connected-react-router';
import { history } from './client/store/history';
import { Provider } from 'react-redux';
import store from './client/store/store';
import { StaticRouter } from 'react-router-dom';

const app = express();

if (process.env.ENABLE_CORS) {
    app.use(cors());
}

app.use('/static', express.static(path.resolve('./static')));

app.get('*', (req, res) => {

    // TODO (alexa): This should pull data from the backend to populate the app
    // prior to rendering (it can be passed in through the store) so that the 
    // requests can be made in parallel and can be cached (especially common things
    // like frequencies and transaction types).

    // TODO (alexa): Add redis store for session storage. Instead of using cookies
    // for auth (current method), the session can be used and validated against
    // the backend. When auth expires, a redirect to /login can be returned.
    const content = ReactDOMServer.renderToString(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <StaticRouter location={req.url} context={{}}>
                    <App />
                </StaticRouter>
            </ConnectedRouter>
        </Provider>
    );

    const indexFile = path.resolve('./index.html');
    fs.readFile(indexFile, 'utf8', (err, data) => {
        if (err) {
            console.error('Something went wrong:', err);
            return res.status(500).send('An error occured while fetching the resource you requested.');
        }

        return res.send(
            data.replace('<div id="root"></div>', `<div id="root">${content}</div>`)
        );
    });
});

app.listen(process.env.PORT, () => {
    console.log(`server started on port ${process.env.PORT}.`);
});
