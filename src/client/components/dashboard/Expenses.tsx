import { push } from "connected-react-router";
import { useMemo, useState } from "react";
import { connect } from "react-redux";
import Expense from "../Expense";
import React from "react";
import IExpense from "../../models/IExpense";
import IFrequency from "../../models/IFrequency";
import { deleteExpense } from "../../store/ledger/actions";
import { MONTHS } from "../../utilities/constants";
import RadioButton, { IRadioOption } from "../RadioButton";
import AppDataState from "../../store/AppDataState";
import { getTotalExpenses } from "../../utilities/expenses";
import { getTimesPerPeriod } from "../../utilities/utilities";
import ExpenseModal from "./ExpenseModal";

// @ts-expect-error - The typescript compiler says that it can't find 
// the module for this import. Webpack still compiles it just fine.
import add from '../../assets/add.png';

interface IExpensesProps {
    expenses: IExpense[];
    frequencies: IFrequency[];
    deleteExpense: typeof deleteExpense;
    push: typeof push;
}

const Expenses = (props: IExpensesProps) => {
    const [monthly, setMonthly] = useState(true);
    const [expense, setExpense] = useState<IExpense | undefined>(undefined);

    const total: number = useMemo(() => getTotalExpenses(props.expenses, props.frequencies, monthly), [props.expenses, props.frequencies, monthly])

    const options: IRadioOption<boolean>[] = [
        { value: true, description: MONTHS[new Date().getMonth()] },
        { value: false, description: new Date().getFullYear().toString() }
    ]

    return (
        <div className="expenses">
            <h1>Expenses</h1>
            <div className="add-expense" onClick={() => props.push('/expense/add')}>
                <img src={add} alt="Add" />
            </div>
            <div className="expenses-list">
                {props.expenses.map(x => <Expense key={x.id} expense={x} times={getTimesPerPeriod(x.lastTriggered, x.frequencyId, props.frequencies, monthly)} onClick={setExpense} />)}
            </div>
            <div className="total">
                <span className="amount">${total.toFixed(2)}</span>
                <span className="text">Total</span>
            </div>
            <div className="term-selector">
                <RadioButton value={monthly} options={options} onChange={(value) => setMonthly(value)} />
            </div>
            <ExpenseModal expense={expense} deleteExpense={props.deleteExpense} frequencies={props.frequencies} close={() => setExpense(undefined)} />
        </div>
    );
}

const mapStateToProps = (state: AppDataState): Partial<IExpensesProps> => {
    return {
        expenses: state.ledger.expenses,
        frequencies: state.ledger.frequencies
    };
}

export default connect(mapStateToProps, { deleteExpense, push })(Expenses as any);



