import React, { useEffect, useMemo, useState } from "react";

// @ts-expect-error - The typescript compiler says that it can't find 
// the module for this import. Webpack still compiles it just fine.
import downArrow from '../assets/down-arrow.png';

export interface IDropdownOption<T> {
    value: T;
    displayValue: string;
}

interface IDropdownProps<T> {
    value: T;
    label: string;
    disabled?: boolean;
    options: IDropdownOption<T>[];
    onChange: (value: T) => void;
}

function Dropdown<T>(props: IDropdownProps<T>): JSX.Element {
    const [focused, setFocused] = useState(false);

    const id = `dropdown-${props.options.map(x => x.displayValue[0]).join('')}`;

    const onLabelClick = () => document.getElementById(id)?.focus();

    const displayValue = useMemo(() => {
        const option: IDropdownOption<T> | undefined = props.options.find(x => x.value === props.value);
        return option === undefined ? '' : option.displayValue;
    }, [props.options, props.value]);

    const containerClasses = useMemo(() => {
        const classes: string[] = ['dropdown'];
        if (focused) { classes.push('open'); }
        return classes.join(' ');
    }, [focused]);

    const labelClasses = useMemo(() => {
        const classes: string[] = [];
        if (focused || !!props.value) { classes.push('active'); }
        return classes.join(' ');
    }, [focused, props.value])


    const onSelect = (value: T) => {
        props.onChange(value);
        setFocused(false);
    }

    return (
        <div className={containerClasses}>
            <label className={labelClasses} onClick={onLabelClick}>{props.label}</label>
            <input id={id} type="text" value={displayValue} readOnly onFocus={() => setFocused(true)} onBlur={() => setFocused(false)} />
            <img className="icon" src={downArrow} alt="" />
            {focused && props.options.length > 0 &&
                <div className="dropdown-menu">
                    {props.options.map(x =>
                        <div key={x.displayValue} className="dropdown-item" onMouseDown={() => onSelect(x.value)}>
                            <span className="text">{x.displayValue}</span>
                        </div>)
                    }
                </div>}
        </div>
    );
}

export default Dropdown;
