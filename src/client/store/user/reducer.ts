import { IUserInfo } from "../../models/IUserInfo";
import { ActionType, UserAction } from "../actions"
import { AppDataPayload } from "../AppDataPayload";

export interface UserState {
    error: string;
    user: IUserInfo | undefined;
    refreshTimeout: NodeJS.Timeout | undefined;
    route: string;
}

const defaultState: UserState = {
    error: '',
    user: undefined,
    refreshTimeout: undefined,
    route: ''
}

export const UserReducer = (state: UserState = defaultState, action: { type: ActionType, payload: AppDataPayload }) => {
    switch (action.type) {
        case UserAction.SET_USER_ERROR:
            state = {
                ...state,
                error: action.payload.errorMessage
            };
            break;
        case UserAction.SET_LOGIN_SUCCESS:
            if (state.refreshTimeout) {
                window.clearTimeout(state.refreshTimeout);
            }
            state = {
                ...state,
                user: action.payload.user,
                refreshTimeout: action.payload.refreshTimeout
            };
            break;
        case UserAction.SET_LOGOUT:
            if (state.refreshTimeout) {
                window.clearTimeout(state.refreshTimeout);
            }
            state = {
                ...state,
                user: undefined,
                refreshTimeout: undefined
            };
            break;
        case UserAction.SET_ROUTE:
            state = {
                ...state,
                route: action.payload.route
            };
            break;
    }
    return state;
}