import React from "react";
import { Link } from "react-router-dom";

const Footer = () =>
    <div className="footer">
        <div className="web">
            <a href="https://gitlab.com/letskeepitcheap/dashboard" target="_blank" rel="noreferrer">Front end</a>
        </div>
        <div className="server">
            <a href="https://gitlab.com/letskeepitcheap/server" target="_blank" rel="noreferrer">Back end</a>
        </div>
        <div className="worker">
            <a href="https://gitlab.com/letskeepitcheap/worker" target="_blank" rel="noreferrer">Worker</a>
        </div>
        <div className="about">
            <Link to="/about">About</Link>
        </div>
    </div>

export default Footer;