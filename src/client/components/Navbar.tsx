import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { IUserInfo } from "../models/IUserInfo";
import UserRole from "../models/UserRole";
import AppDataState from "../store/AppDataState";
import { logout } from "../store/user/actions";

// TODO (alexa): This "works" but not in a great way. When on a non-dashboard page,
// it's not intuitive that one can return to the dashboard by clicking the title.

// TODO (alexa): Add a dropdown for adding ledger entries, expenses, and income 
// generators so that the + buttons can be removed from their respective components.

interface INavbarProps {
    user: IUserInfo;
    logout: typeof logout;
}

const Navbar = (props: INavbarProps) =>
    <div className="navbar">
        <h1 className="title">
            <Link to="/dashboard">letskeepit.cheap</Link>
        </h1>
        <div className="links">
            <div className="nav-login">
                {props.user ?
                    <Link to="/" onClick={() => props.logout()}>
                        Log out
                    </Link>
                    : <Link to="/login" >
                        Log in
                    </Link>
                }
            </div>
            {props.user && props.user.role === UserRole.Admin &&
                <div className="nav-admin">
                    <Link to="/dashboard/admin">Admin</Link>
                </div>
            }
        </div>
    </div>;


const mapStateToProps = (state: AppDataState): Partial<INavbarProps> => {
    return {
        user: state.user.user
    };
}

export default connect(mapStateToProps, { logout })(Navbar as any);