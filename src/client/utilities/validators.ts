import { MAXIMUM_CATEGORY_LENGTH, MAXIMUM_DESCRIPTION_LENGTH, MINIMUM_PASSWORD_LENGTH } from "./constants";

export const numericValidator = (value: string): boolean => isNaN(parseFloat(value));

export const passwordValidator = (value: string): boolean => value.length < MINIMUM_PASSWORD_LENGTH;

export const descriptionValidator = (value: string): boolean => value.length > MAXIMUM_DESCRIPTION_LENGTH;

export const categoryValidator = (value: string): boolean => value.length > MAXIMUM_CATEGORY_LENGTH;
