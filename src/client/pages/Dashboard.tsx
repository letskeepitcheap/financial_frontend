import { push } from "connected-react-router";
import React, { useRef } from "react";
import { connect } from "react-redux";
import Button from "../components/Button";
import Expenses from "../components/dashboard/Expenses";
import Income from "../components/dashboard/Income";
import LedgerHistory from "../components/dashboard/LedgerHistory";
import LedgerHistoryGraph from "../components/dashboard/LedgerHistoryGraph";
import IFrequency from "../models/IFrequency";
import AppDataState from "../store/AppDataState";
import { getFrequencies, getIncomeGenerators, getLedgerEntries, getExpenses } from "../store/ledger/actions";
import { usesFrequencies, usesIncomeGenerators, usesLedgerEntries, usesExpenses } from "../utilities/hooks";
//import mail from '../assets/mail.png';

interface IDashboardProps {
    username: string;
    frequencies: IFrequency[];
    getLedgerEntries: typeof getLedgerEntries;
    getIncomeGenerators: typeof getIncomeGenerators;
    getExpenses: typeof getExpenses;
    getFrequencies: typeof getFrequencies;
    push: typeof push;
}

const Dashboard = (props: IDashboardProps) => {

    usesFrequencies(props.frequencies, props.getFrequencies);
    usesLedgerEntries(props.getLedgerEntries);
    usesIncomeGenerators(props.getIncomeGenerators);
    usesExpenses(props.getExpenses);

    return (
        <div className="dashboard">
            <h1 className="welcome">
                <label htmlFor="user-name">Name</label>
                <span id="user-name" className="name">{props.username}</span>
            </h1>
            <div className="inbox">
                {/* <img src={mail} alt="Inbox" onClick={() => props.push('/inbox')} /> */}
            </div>
            <LedgerHistoryGraph />
            <LedgerHistory />
            <Income />
            <Expenses />
        </div>
    );
}

const mapStateToProps = (state: AppDataState): Partial<IDashboardProps> => {
    return {
        username: state.user.user!.username,
        frequencies: state.ledger.frequencies
    };
}

export default connect(mapStateToProps, { getLedgerEntries, getIncomeGenerators, getExpenses, getFrequencies, push })(Dashboard as any);