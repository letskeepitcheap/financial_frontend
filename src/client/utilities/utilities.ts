import IExpense from '../models/IExpense';
import IFrequency from '../models/IFrequency';
import { getTimesPerMonth, getTimesPerMonthFromLastTriggeredAndFrequency, getTimesPerYear, getTimesPerYearFromLastTriggeredAndFrequency } from './dates';

export const selectUnique = (array: string[]): string[] =>
    [...array].filter((v, i, self) => self.indexOf(v) === i);


export const sortFrequencies = (array: IFrequency[]): IFrequency[] =>
    [...array].sort((a, b) => {
        if (a.approxTimesPerYear > b.approxTimesPerYear) {
            return 1;
        } else if (b.approxTimesPerYear > a.approxTimesPerYear) {
            return -1;
        }
        return 0;
    });


export const getAmountAndTimes = (expense: IExpense, frequencies: IFrequency[], monthly: boolean): { total: number, times: string } =>
    monthly ? getTimesPerMonth(expense.lastTriggered, expense.frequencyId, frequencies, expense.amount)
        : getTimesPerYear(expense.lastTriggered, expense.frequencyId, frequencies, expense.amount);

export const getTimesPerPeriod = (lastTriggered: Date, frequencyId: string, frequencies: IFrequency[], monthly: boolean): number =>
    monthly ? getTimesPerMonthFromLastTriggeredAndFrequency(lastTriggered, frequencyId, frequencies)
        : getTimesPerYearFromLastTriggeredAndFrequency(lastTriggered, frequencyId, frequencies);

export function debounce<Params extends any[]>(func: (...args: Params) => void, ms = 1000): (...args: Params) => void {
    let timer: NodeJS.Timeout;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => func(...args), ms);
    }
}

