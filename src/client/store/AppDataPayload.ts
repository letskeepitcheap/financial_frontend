import IExpense from "../models/IExpense";
import IFrequency from "../models/IFrequency";
import IIncomeGenerator from "../models/IIncomeGenerator";
import ILedgerEntry from "../models/ILedgerEntry";
import ITransactionType from "../models/ITransactionType";
import { IUserInfo } from "../models/IUserInfo";

export interface AppDataPayload {
    errorMessage: string;
    user: IUserInfo;
    refreshTimeout: NodeJS.Timeout;
    route: string;
    categories: string[];
    frequencies: IFrequency[];
    transactionTypes: ITransactionType[];
    incomeGenerators: IIncomeGenerator[];
    entries: ILedgerEntry[];
    expenses: IExpense[];
    id: string;
}