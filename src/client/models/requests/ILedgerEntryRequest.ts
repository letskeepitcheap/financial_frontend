export default interface ILedgerEntryRequest {
    category: string;
    description: string;
    amount: number;
    transactionTypeId: string;
    recurringTransactionId: string;
    transactionDate: Date;
}