export default interface IExpenseRequest {
    category: string;
    description: string;
    amount: number;
    frequencyId: string;
    transactionTypeId: string;
    lastTriggered: Date;
}