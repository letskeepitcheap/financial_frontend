import { fireEvent, render, RenderResult, waitFor } from "@testing-library/react"
import '@testing-library/jest-dom';
import React from "react";
import AutocompleteField from '../../../client/components/AutocompleteField';

describe('AutocompleteField', () => {
    let utils: RenderResult;

    const defaultProps = {
        label: 'label',
        options: ['one', 'two'],
        value: 'one',
        onChange: () => undefined,
        getOptions: () => undefined,
        validator: () => false
    };

    const onChangeSpy = jest.spyOn(defaultProps, 'onChange');
    const getOptionsSpy = jest.spyOn(defaultProps, 'getOptions');
    const validatorSpy = jest.spyOn(defaultProps, 'validator');

    beforeEach(() => {
        utils = render(<AutocompleteField {...defaultProps} />);
        jest.resetAllMocks();
    });

    afterEach(() => {
        jest.useRealTimers();
    });

    it('should have expected structure', () => {
        const { container } = utils;

        expect(container.firstChild).toHaveClass('dropdown');

        const dropdown = container.firstChild!;
        expect(dropdown.childNodes.length).toBe(2);

        expect(dropdown.childNodes[1]).toHaveValue(defaultProps.value);
    });

    it('should call onChange when input is changed', () => {
        jest.useFakeTimers();
        const { container } = utils;

        const input = container.firstChild?.childNodes[1]!;
        fireEvent.change(input, { target: { value: 'text' } });

        jest.runAllTimers();

        expect(onChangeSpy).toHaveBeenCalledTimes(1);
        expect(getOptionsSpy).toHaveBeenCalledTimes(1);
    });

    it('should call validator on input blur', () => {
        const { container } = utils;

        const input = container.firstChild?.childNodes[1]!;
        fireEvent.blur(input);
        expect(validatorSpy).toHaveBeenCalledTimes(1);
    });

    it('should open menu when focused with options', () => {
        const { container } = utils;

        const dropdown = container.firstChild!;
        fireEvent.focus(dropdown);

        expect(dropdown.childNodes.length).toBe(3);

        const menu = dropdown.childNodes[2];
        expect(menu).toHaveClass('dropdown-menu');
        expect(menu.childNodes.length).toBe(defaultProps.options.length);
    });
});