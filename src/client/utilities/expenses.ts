import IExpense from "../models/IExpense";
import IFrequency from "../models/IFrequency";
import { getTimesPerPeriod } from "./utilities";

export const expensesWithoutGenerators = (expenses: IExpense[]): IExpense[] =>
    expenses.filter(x => !x.incomeGeneratorId)
        .sort((a, b) => {
            if (a.category > b.category) {
                return 1;
            } else if (b.category > a.category) {
                return -1;
            }
            return 0;
        });

export const getTotalExpenses = (expenses: IExpense[], frequencies: IFrequency[], monthly: boolean): number => {
    if (expenses.length === 0) {
        return 0;
    }
    return expenses
        .map(x => getTimesPerPeriod(x.lastTriggered, x.frequencyId, frequencies, monthly) * (x.transactionType === 'Income' ? x.amount : -x.amount))
        .reduce((sum, x) => sum + x);
}