interface IIncome {
    net: number;
    gross: number;
}

export default IIncome;