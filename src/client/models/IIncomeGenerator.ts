import IExpense from "./IExpense";

export default interface IIncomeGenerator {
    id: string;
    description: string;
    frequencyId: string;
    recurringTransactions: IExpense[];
}