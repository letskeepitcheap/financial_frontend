import { AnyAction } from 'redux';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { AxiosError } from 'axios';
import { IUserInfo } from '../../models/IUserInfo';
import IUserRequest from '../../models/requests/IUserRequest';
import { StoreAction, UserAction } from '../actions';
import { del, get, post, put } from '../../utilities/backend_client';
import AppDataState from '../AppDataState';

const setUserError = (error: AxiosError): StoreAction => {
    if (!error.response) {
        return { type: UserAction.SET_USER_ERROR, payload: { errorMessage: 'Unknown error' } };
    }

    const getErrorMessage = (code: number): string => {
        switch (code) {
            case 400: return 'Incorrect usrname or password';
            case 401: return 'Incorrect username or password.';
            case 404: return `Couldn't reach the server, try again soon.`;
            case 500: return 'Something went wrong. Try again soon';
            default: return 'Unknown error'
        }
    }

    return {
        type: UserAction.SET_USER_ERROR,
        payload: {
            errorMessage: getErrorMessage(error.response?.status)
        }
    };
}

// ThunkAction<return-type, state, extra-thunk-arg, basic-action.
export const clearUserError = (): ThunkAction<void, AppDataState, undefined, AnyAction> => dispatch =>
    dispatch({ type: UserAction.SET_USER_ERROR, payload: { errorMessage: '' } });

const setLoggedIn = (refreshFunc: () => ThunkAction<Promise<void>, AppDataState, undefined, AnyAction>): (user: IUserInfo) => StoreAction =>
    (user: IUserInfo): StoreAction => ({
        type: UserAction.SET_LOGIN_SUCCESS, payload: {
            user, refreshTimeout: setTimeout(() => {
                refreshFunc();
            }, 10 * 60 * 1000)
        }
    });

const setLogout = (): StoreAction => ({ type: UserAction.SET_LOGOUT, payload: {} });

export const refreshLogin = (): ThunkAction<Promise<void>, AppDataState, undefined, AnyAction> =>
    async (dispatch: ThunkDispatch<AppDataState, undefined, AnyAction>): Promise<void> => {
        console.log('refreshing login');
        const response = await get('user/login', setLoggedIn(refreshLogin), setLogout);
        console.log(response.type);
        dispatch(response);
    }

export const createUser = (request: IUserRequest): ThunkAction<Promise<void>, AppDataState, undefined, AnyAction> =>
    async (dispatch: ThunkDispatch<AppDataState, undefined, AnyAction>): Promise<void> => {
        const response = await post(request, 'user/create', setLoggedIn(refreshLogin), setUserError)
        dispatch(response);
    }

export const login = (request: IUserRequest): ThunkAction<Promise<void>, AppDataState, undefined, AnyAction> =>
    async (dispatch: ThunkDispatch<AppDataState, undefined, AnyAction>): Promise<void> => {
        const response = await put(request, 'user/login', setLoggedIn(refreshLogin), setUserError);
        dispatch(response);
    };

export const logout = (): ThunkAction<Promise<void>, AppDataState, undefined, AnyAction> =>
    async (dispatch: ThunkDispatch<AppDataState, undefined, AnyAction>): Promise<void> => {
        await del('user/logout');
        dispatch(setLogout());
    }

export const setRoute = (route: string): ThunkAction<void, AppDataState, undefined, AnyAction> => dispatch =>
    dispatch({ type: UserAction.SET_ROUTE, payload: { route } });


