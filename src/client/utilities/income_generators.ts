import IFrequency from "../models/IFrequency";
import IIncomeGenerator from "../models/IIncomeGenerator";
import { getTimesPerPeriod } from "./utilities";

export const calculateIncome = (generator: IIncomeGenerator, frequencies: IFrequency[], monthly: boolean): { net: number, gross: number } => {
    if (frequencies.length === 0 || generator.recurringTransactions.length === 0) {
        return { net: 0, gross: 0 };
    }

    const getAmountPerPeriod = (value: number, transactionType: string): { net: number, gross: number } =>
        transactionType === "Income" ? { net: value, gross: value } : { net: -value, gross: 0 };

    return generator.recurringTransactions
        .map(x => {
            const income = getAmountPerPeriod(x.amount, x.transactionType);
            const transactionCount = getTimesPerPeriod(generator.recurringTransactions[0].lastTriggered, generator.frequencyId, frequencies, monthly);
            return {
                net: income.net * transactionCount,
                gross: income.gross * transactionCount
            };
        })
        .reduce((sum, x) => {
            sum.net += x.net;
            sum.gross += x.gross;
            return sum;
        });
}


export const getTotalIncomeGenerators = (generators: IIncomeGenerator[], frequencies: IFrequency[], monthly: boolean): { net: number, gross: number } => {
    if (generators.length === 0) {
        return { net: 0, gross: 0 };
    }
    return generators.map(x => calculateIncome(x, frequencies, monthly))
        .reduce((sum, x) => {
            sum.net += x.net;
            sum.gross += x.gross;
            return sum;
        });
}