import React from "react";
import IFrequency from "../../models/IFrequency";
import IIncomeGenerator from "../../models/IIncomeGenerator";
import { deleteIncomeGenerator } from "../../store/ledger/actions";
import { getTimesPerPeriod } from "../../utilities/utilities";
import Button from "../Button";
import Expense from "../Expense";
import Modal from "../Modal";


interface IIncomeGeneratorModalProps {
    generator?: IIncomeGenerator;
    frequencies: IFrequency[];
    deleteIncomeGenerator: typeof deleteIncomeGenerator;
    close: () => void;
}

const IncomeGeneratorModal = (props: IIncomeGeneratorModalProps) => {

    const getFrequencyDescription = (): string => {
        const selected = props.frequencies.find(x => x.id === props.generator!.frequencyId);
        if (!selected) {
            props.close();
            return '';
        }
        return selected.description;
    }

    const onDeleteClick = async () => {
        props.deleteIncomeGenerator(props.generator!.id);
        props.close();
    }

    return props.generator ?
        <Modal show={!!props.generator} close={props.close}>
            <h1>{props.generator!.description}</h1>
            <div className="frequency">
                {getFrequencyDescription()}
            </div>
            <h2>Deductions</h2>
            <div className="deductions">
                {props.generator!.recurringTransactions.map(x =>
                    <Expense key={x.id} expense={x} times={getTimesPerPeriod(x.lastTriggered, x.frequencyId, props.frequencies, true)} />
                )}
            </div>
            <Button onClick={onDeleteClick} error>
                Delete
            </Button>
        </Modal>
        : null;
}

export default IncomeGeneratorModal;