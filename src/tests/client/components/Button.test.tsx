import { fireEvent, render, RenderResult } from "@testing-library/react";
import React from "react";
import Button from '../../../client/components/Button';
import '@testing-library/jest-dom';

describe('Button', () => {
    let utils: RenderResult;

    const defaultProps = {
        onClick: () => undefined,
        disabled: false,
        error: false
    };

    const spy = jest.spyOn(defaultProps, 'onClick');

    beforeEach(() => {
        utils = render(
            <Button {...defaultProps}>
                <img src='test' alt="test-alt" />
            </Button>
        );
        jest.resetAllMocks();
    });

    it('should show children', () => {
        const { getByAltText } = utils;

        expect(getByAltText(/test-alt/i)).toBeVisible();
    });

    it('should be disabled when disabled', () => {
        utils.unmount();
        const { container } = render(<Button {...{ ...defaultProps, disabled: true }} />);

        expect(container.firstChild).toHaveAttribute('disabled');
    });

    it('should have error class when error', () => {
        utils.unmount();
        const { container } = render(<Button {...{ ...defaultProps, error: true }} />);

        expect(container.firstChild).toHaveClass('error');
    });

    it('should call onClick when clicked', () => {
        const { container } = utils;

        fireEvent.click(container.firstChild);

        expect(spy).toHaveBeenCalledTimes(1);
    });
});